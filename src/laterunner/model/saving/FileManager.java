package laterunner.model.saving;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import laterunner.model.shop.Shop;
import laterunner.model.user.UserStatistics;
import laterunner.model.user.User;

/**
 * Class which contains functions to save and load user's info.
 */
public final class FileManager {

    private static final String FILE = "file_manager.txt";

    private FileManager() { }

    /**
     * Saves user's info to file.
     */
    public static void saveToFile() {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(FILE))) {
            for (String field : getFieldsList()) {
                writer.write(field);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } 
    }

    private static List<String> getFieldsList() {
        List<String> list = new ArrayList<>();
        list.add("" + User.getInstance().getMoney());
        list.add("" + User.getInstance().getUserLives());
        list.add("" + User.getInstance().getLevelReached());
        list.add("" + User.getInstance().getSpeedMultiplier());
        UserStatistics userStatistics = User.getInstance().getStatistics();
        list.add("" + userStatistics.getGamesPlayed());
        list.add("" + userStatistics.getLostLives());
        list.add("" + userStatistics.getSurvivalScore());
        list.add("" + userStatistics.getMotorbikeHits());
        list.add("" + userStatistics.getCarHits());
        list.add("" + userStatistics.getTruckHits());
        list.add("" + Shop.getInstance().getLifeCost());
        list.add("" + Shop.getInstance().getSpeedCost());
        return list;
    }

    /**
     * Loads user's info from file.
     */
    public static void loadFromFile() {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(FILE))) {
            setFieldsList(reader.lines().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void setFieldsList(final List<String> fieldsList) {
        Iterator<String> iterator = fieldsList.iterator();
        User.getInstance().setMoney(Integer.parseInt(iterator.next()));
        User.getInstance().setUserLives(Integer.parseInt(iterator.next()));
        User.getInstance().setLevelReached(Integer.parseInt(iterator.next()));
        User.getInstance().setSpeedMultiplier(Double.parseDouble(iterator.next()));
        UserStatistics userStatistics = User.getInstance().getStatistics();
        userStatistics.setGamesPlayed(Long.parseLong(iterator.next()));
        userStatistics.setLostLives(Long.parseLong(iterator.next()));
        userStatistics.setSurvivalScore(Long.parseLong(iterator.next()));
        userStatistics.setMotorbikeHits(Long.parseLong(iterator.next()));
        userStatistics.setCarHits(Long.parseLong(iterator.next()));
        userStatistics.setTruckHits(Long.parseLong(iterator.next()));
        Shop.getInstance().setLifeCost(Integer.parseInt(iterator.next()));
        Shop.getInstance().setSpeedCost(Integer.parseInt(iterator.next()));
    }
}
