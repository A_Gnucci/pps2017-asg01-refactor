package laterunner.graphics.panel;

import laterunner.graphics.PictureManagerImpl;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Panel Implementation.
 */
abstract class PanelImpl extends JPanel {

    /**
     * 
     */
    private final PictureManagerImpl pics = new PictureManagerImpl();

    protected JButton createButton(final ImageIcon img) {
        JButton button = new JButton(img);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        return button;
    }

    protected Font createFont(final float size) {
        try (InputStream in = StatisticsPanel.class.getResourceAsStream("/Digital Dot Roadsign.otf")) {
            Font font = Font.createFont(Font.TRUETYPE_FONT, in).deriveFont(size);
            GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
            graphicsEnvironment.registerFont(font);
            return font;
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
            return this.getFont();
        }
    }

    /**
     * Gets an instance of IconsFunction.
     * 
     * @return
     *          an instance of IconsFunction class
     */
    protected PictureManagerImpl getPics() {
        return this.pics;
    }
}