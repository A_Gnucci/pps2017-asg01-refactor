package laterunner.test;

import static org.junit.Assert.assertEquals;

import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.PlayerCar;
import laterunner.model.vehicle.VehicleFactoryImpl;
import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Position2DImpl;
import laterunner.physics.Speed2DImpl;
import org.junit.Test;

/**
 * Tests VehicleImpl's method.
 */
public class VehicleImplTest {

    private static final int X_POSITION = 435;

    @Test
    public void testCarType(){
        PlayerCar car = new PlayerCar();
        assertEquals(car.getType(), VehicleType.USER_CAR);
    }

    @Test
    public void testCarPosition(){
        final int Y_POSITION = 510;
        PlayerCar car = new PlayerCar();
        Position2DImpl position = new Position2DImpl(X_POSITION, Y_POSITION);
        assertEquals(car.getPosition().getX(), position.getX(), 0.0);
        assertEquals(car.getPosition().getY(), position.getY(), 0.0);
    }

    @Test
    public void testCarSpeed(){
        PlayerCar car = new PlayerCar();
        car.setSpeed(new Speed2DImpl(1, 1));
        assertEquals(1, car.getSpeed().getXSpeed(), 0.0);
        assertEquals(0, car.getSpeed().getYSpeed(), 0.0);
    }

    @Test
    public void testObstacle(){
        final int BUS_MALUS = 3000;
        Obstacle obstacle = new VehicleFactoryImpl().createObstacleBus(new Position2DImpl(X_POSITION, 1),
                new Speed2DImpl(0, 1));
        assertEquals(obstacle.getLifeDamage(), 3);
        assertEquals(obstacle.getMalus(), BUS_MALUS);
        assertEquals(0, obstacle.getSpeed().getXSpeed(), 0.0);
        assertEquals(1, obstacle.getSpeed().getYSpeed(), 0.0);
        assertEquals(obstacle.getType(), VehicleType.BUS);
    }
}
