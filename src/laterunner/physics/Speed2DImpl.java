package laterunner.physics;

/**
 * The class in witch is implemented the concept of speed of the VehicleType.
 *
 */
public class Speed2DImpl implements Speed2D {

    private static final long serialVersionUID = 1L;

    private final double xSpeed;
    private final double ySpeed;

    /**
     * 
     * @param xSpeed
     *          the X component of speed
     * @param ySpeed
     *          the ySpeed component of speed
     */
    public Speed2DImpl(final double xSpeed, final double ySpeed) {
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    /**
     * Multiply the speed by factor.
     * 
     * @param factor
     *          parameter to multiply speed to
     * @return
     *          speed multiply factor
     */
    public Speed2D multiply(final double factor) {
        return new Speed2DImpl(xSpeed * factor, ySpeed * factor);
    }

    @Override
    public String toString() {
        return "Speed2DImpl(" + xSpeed + "," + ySpeed + ")";
    }

    /**
     * Get the X component.
     * 
     * @return
     *          the X component
     */
    public double getXSpeed() {
        return xSpeed;
    }

    /**
     * Get the Y component.
     * 
     * @return
     *          the Y component
     */
    public double getYSpeed() {
        return ySpeed;
    }
}