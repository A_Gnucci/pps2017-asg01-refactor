package laterunner.model.vehicle;

import laterunner.model.collisions.BoundingBox;

public interface Obstacle extends Vehicle{

    /**
     * Returns vehicle's type.
     *
     * @return
     *          vehicle's type
     */
    VehicleType getType();

    /**
     * Returns obstacle's malus.
     *
     * @return
     *          obstacle's malus
     */
    int getMalus();

    /**
     * Returns obstacle's life damage.
     *
     * @return
     *          obstacle's life damage
     */
    int getLifeDamage();

    /**
     * Returns obstacle's bounding box.
     *
     * @return
     *          obstacle's bounding box
     */
    BoundingBox getBoundingBox();
}
