package laterunner.model.vehicle;

import laterunner.model.collisions.BoundingBox;
import laterunner.model.collisions.ObstacleBoundingBox;
import laterunner.physics.Position2D;
import laterunner.physics.Speed2D;
import laterunner.physics.Speed2DImpl;

/**
 * Game obstacle.
 */
class ObstacleImpl extends VehicleImpl implements Obstacle {

    private static final int CAR_MALUS = 1000;
    private static final int CAR_LIFE_DAMAGE = 1;
    private static final int BUS_MALUS = 3000;
    private static final int BUS_LIFE_DAMAGE = 3;
    private static final int MOTORBIKE_MALUS = 2000;
    private static final int MOTORBIKE_LIFE_DAMAGE = 0;

    private final int malus;
    private final int lifeDamage;
    private final BoundingBox boundingBox;

    /**
     * Instantiates a new ObstacleImpl.
     * 
     * @param vehicle
     *          obstacle's type
     * @param position
     *          obstacle starting position
     * @param speed
     *          obstacle starting speed
     */
    ObstacleImpl(final VehicleType vehicle, final Position2D position, final Speed2D speed) {
        super(position, speed, vehicle);

        switch(super.getType()) {
        case OBSTACLE_CAR:
            this.malus = CAR_MALUS;
            this.lifeDamage = CAR_LIFE_DAMAGE;
            break;
        case BUS:
            this.malus = BUS_MALUS;
            this.lifeDamage = BUS_LIFE_DAMAGE;
            break;
        case MOTORBIKE:
            this.malus = MOTORBIKE_MALUS;
            this.lifeDamage = MOTORBIKE_LIFE_DAMAGE;
            break;
        default: throw new IllegalArgumentException("ObstacleImpl with user car feature");
        }
        this.boundingBox = new ObstacleBoundingBox(this.getPosition().getX(), super.getType());
    }

    /**
     * Returns obstacle's malus.
     * 
     * @return
     *          obstacle's malus
     */
    public int getMalus() {
        return this.malus;
    }

    /**
     * Returns obstacle's life damage.
     * 
     * @return
     *          obstacle's life damage
     */
    public int getLifeDamage() {
        return this.lifeDamage;
    }

    /**
     * Returns obstacle's bounding box.
     * 
     * @return
     *          obstacle's bounding box
     */
    public BoundingBox getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    public void setSpeed(final Speed2D speed) {
        super.setCheckedSpeed(new Speed2DImpl(0, speed.getYSpeed()));
    }
}
