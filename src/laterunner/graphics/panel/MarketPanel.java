package laterunner.graphics.panel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import laterunner.graphics.Icon;
import laterunner.graphics.SceneImpl;
import laterunner.model.shop.Shop;
import laterunner.model.user.User;

/**
 * MarketPanel class provides the possibility of buy speed and lives in order to move ahead in the game.
 *
 */
public class MarketPanel extends PanelImpl {

    private static final long serialVersionUID = 1L;
    private static final float FONT_SIZE = 48;
    private static final int MAX_SPEED_COST = 12000;
    private final JLabel lifeCostLabel;
    private final JLabel moneyCostLabel;
    private JButton lifeButton;
    private JButton coinButton;

    /**
     * Common MarketPanel constructor. Sets the panel's layout and creates some buttons in order to display
     * every info about lives and money.
     */
    public MarketPanel() {
        this.setLayout(new GridLayout(4, 2));
        Font font = super.createFont(FONT_SIZE);
        setupButtons(font);
        setupLabels(font);

        //Life and Money costs
        lifeCostLabel = new JLabel("         Life Cost: " + Shop.getInstance().getLifeCost());
        lifeCostLabel.setFont(font);
        lifeCostLabel.setForeground(Color.WHITE);
        moneyCostLabel = new JLabel("        Speed Cost: " + Shop.getInstance().getSpeedCost());
        moneyCostLabel.setFont(font);
        moneyCostLabel.setForeground(Color.WHITE);

        this.add(lifeCostLabel);
        this.add(moneyCostLabel);
    }

    private void setupButtons(Font font){
        JButton backButton = super.createButton(super.getPics().getIcon(Icon.BACK)); //Back button
        backButton.addActionListener(e -> SceneImpl.changePanel("menu"));
        JButton buyLifeButton = super.createButton(super.getPics().getIcon(Icon.BUY_LIFE)); //Buy lifeButton button
        buyLifeButton.addActionListener(e -> Shop.getInstance().buyLife());
        JButton buySpeedButton = super.createButton(super.getPics().getIcon(Icon.BUY_SPEED)); //Buy speed button
        buySpeedButton.addActionListener(e -> Shop.getInstance().buySpeed());

        this.lifeButton = super.createButton(super.getPics().getIcon(Icon.HEART)); //Life button
        lifeButton.setText("" + User.getInstance().getUserLives());
        lifeButton.setFont(font);
        lifeButton.setHorizontalTextPosition(SwingConstants.CENTER);

        this.coinButton = super.createButton(super.getPics().getIcon(Icon.COIN)); //Coin button
        coinButton.setText("" + User.getInstance().getMoney());
        coinButton.setFont(font);
        coinButton.setHorizontalTextPosition(SwingConstants.CENTER);

        this.add(buyLifeButton);
        this.add(buySpeedButton);
        this.add(backButton);
    }

    private void setupLabels(Font font){
        Box livesBox = Box.createHorizontalBox();
        JLabel livesLabel = new JLabel("     Your lives:");
        livesLabel.setFont(font);
        livesLabel.setForeground(Color.WHITE);
        livesBox.add(livesLabel);
        livesBox.add(lifeButton);
        this.add(livesBox);

        Box moneyBox = Box.createHorizontalBox();
        JLabel moneyLabel = new JLabel("    Your money:");
        moneyLabel.setFont(font);
        moneyLabel.setForeground(Color.WHITE);
        moneyBox.add(moneyLabel);
        moneyBox.add(coinButton);
        this.add(moneyBox);
    }

    @Override
    protected void paintComponent(final Graphics graphics) {
        graphics.drawImage(super.getPics().getImage(Icon.BACKGROUND), 0, 0, null);
        this.lifeButton.setText("" + User.getInstance().getUserLives());
        this.lifeCostLabel.setText("         Life Cost: " + Shop.getInstance().getLifeCost());
        this.coinButton.setText("" + User.getInstance().getMoney());
        if (Shop.getInstance().getSpeedCost() >= MAX_SPEED_COST) {
            this.moneyCostLabel.setText("        Speed Max");
        } else {
            this.moneyCostLabel.setText("        Speed Cost: " + Shop.getInstance().getSpeedCost());
        }
    }
}