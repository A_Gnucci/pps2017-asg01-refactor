package laterunner.graphics;

import java.awt.Image;
import java.util.EnumMap;
import java.util.Map;

import javax.swing.ImageIcon;

/**
 * PictureManager Implementation.
 * 
 */
public class PictureManagerImpl implements PictureManager {

    private final Map<Icon, ImageIcon> icons = new EnumMap<>(Icon.class);
    private final Map<Icon, Image> images = new EnumMap<>(Icon.class);

    /**
     * Common IconsFunction constructor. Initializes the maps containing all the images.
     */
    public PictureManagerImpl() {
        for (Icon i:Icon.values()) {
            icons.put(i, getImageIcon(i));
            images.put(i, getImageFromIcon(i));
        }
    }

    private static ImageIcon getImageIcon(final Icon icon) {
        return new ImageIcon(Icon.class.getResource(getPath(icon)));
    }

    private static Image getImageFromIcon(final Icon icon) {
        return new ImageIcon(Icon.class.getResource(getPath(icon))).getImage();
    }

    private static String getPath(final Icon icon) {
        String path;
        switch(icon) {
        case BACK:
            path = "/back.png";
            break;
        case BACKGROUND:
            path = "/background.jpg";
            break;
        case BUS:
            path = "/bus.png";
            break;
        case BUY_LIFE:
            path = "/buy_life.png";
            break;
        case BUY_SPEED:
            path = "/buy_speed.png";
            break;
        case CAR:
            path = "/car.png";
            break;
        case COIN:
            path = "/coin.png";
            break;
        case CROSS:
            path = "/cross.png";
            break;
        case HEART:
            path = "/heart.png";
            break;
        case JEEP:
            path = "/jeep.png";
            break;
        case MENU:
            path = "/menu.png";
            break;
        case MOTORBIKE:
            path = "/motorbike.png";
            break;
        case PLAY:
            path = "/play.png";
            break;
        case QUIT:
            path = "/quit.png";
            break;
        case ROAD:
            path = "/road.png";
            break;
        case SHOP:
            path = "/shop.png";
            break;
        case STATS:
            path = "/stats.png";
            break;
        default:
            throw new IllegalStateException();
        }
        return path;
    }

    @Override
    public ImageIcon getIcon(final Icon icon) {
        return icons.get(icon);
    }

    @Override
    public Image getImage(final Icon icon) {
        return images.get(icon);
    }
}