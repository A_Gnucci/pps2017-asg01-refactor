package laterunner.model.vehicle;

import laterunner.core.Dimensions;
import laterunner.model.collisions.BorderBoundingBox;
import laterunner.physics.Position2D;
import laterunner.physics.Speed2D;

/**
 * Template of a vehicle.
 */
public abstract class VehicleImpl implements Vehicle {

    private static final double SPEED_MULTIPLIER = 0.001;

    private Position2D position;
    private Speed2D speed;
    private VehicleType type;

    /**
     * VehicleType' main constructor.
     * 
     * @param position
     *          vehicle's initial position
     * @param speed
     *          vehicle's initial speed
     * @param vehicleType
     *          vehicle's type
     */
    VehicleImpl(final Position2D position, final Speed2D speed, final VehicleType vehicleType) {
        assert (!(position.getX() < BorderBoundingBox.getInstance().getUpperLeft().getX())
                || !(position.getX() + Dimensions.getInstance().getVehicleWidth(vehicleType) >
                BorderBoundingBox.getInstance().getBelowRight().getX())) &&
                (vehicleType != VehicleType.USER_CAR || !(speed.getYSpeed() != 0)) &&
                (vehicleType == VehicleType.USER_CAR || !(speed.getXSpeed() != 0));
        this.position = position;
        this.speed = speed;
        this.type = vehicleType;
    }

    /**
     * Sets vehicle's position.
     * 
     * @param position
     *          vehicle's new position
     */
    public void setPosition(final Position2D position) {
        assert !(position.getX() < BorderBoundingBox.getInstance().getUpperLeft().getX())
                || !(position.getX() + Dimensions.getInstance().getVehicleWidth(this.getType())
                > BorderBoundingBox.getInstance().getBelowRight().getX());
        this.position = position;
    }

    /**
     * Recalls the checking setter.
     * 
     * @param speed
     *          unchecked vehicle's new speed
     */
    public abstract void setSpeed(final Speed2D speed);

    /**
     * Sets the vehicles's speed after checking it.
     * 
     * @param speed
     *          checked vehicle's new speed
     */
    protected void setCheckedSpeed(final Speed2D speed) {
        this.speed = speed;
    }

    /**
     * Calculates new vehicle's position depending on the speed.
     * 
     * @param elapsed
     *          time elapsed between two frames
     */
    public void updateState(final int elapsed) {
        this.position = this.position.sum(speed.multiply(SPEED_MULTIPLIER * elapsed));
    }

    /**
     * Returns vehicle's current position.
     * 
     * @return
     *          vehicle's current position
     */
    public Position2D getPosition() {
        return this.position;
    }

    /**
     * Returns vehicle's current speed.
     * 
     * @return
     *          vehicle's current speed
     */
    public Speed2D getSpeed() {
        return this.speed;
    }

    /**
     * Returns vehicle's type.
     * 
     * @return
     *          vehicle's type
     */
    public VehicleType getType() {
        return this.type;
    }
}