package laterunner.model.world;

import java.util.List;

import laterunner.model.collisions.WorldEventListener;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.Vehicle;

/**
 * The interface in witch is defined the world features.
 *
 */
public interface World {

    /**
     *  Set the WorldEventListener.
     *
     * @param eventListener
     *          World event listener
     */
    void setEventListener(final WorldEventListener eventListener);

    /**
     * Remove an obstacle from the obstacle list. 
     * 
     * @param obstacle
     *          Obstacle to be removed
     */
    void removeObstacle(final Obstacle obstacle);

    /**
     * Returns scene's entities' list.
     * 
     * @return
     *          scene's entities' list
     */
    List<Vehicle> getSceneEntities();

    /**
     * Updtate the world entities.
     * 
     * @param elapsed
     *          Time elapsed
     */
    void updateState(final int elapsed);

    /**
     * Generete the level by the level's number.
     * 
     * @param i
     *          Level's number
     */
    void generateLevel(final int i);

    /**
     * Get the User's car.
     * 
     * @return
     *          User's car
     */
    Vehicle getPlayerCar();

    /**
     *  Get the border damage.
     * @return
     *          return the border damage
     */
    int getBorderDamage();

}