package laterunner.core;

import laterunner.input.Controller;
import laterunner.model.collisions.WorldEventListener;

public interface GameEngine extends Controller, WorldEventListener {

    /**
     * Loads main settings and shows main menu.
     */
    void initialize();

    /**
     * Loads level.
     *
     * @param levelNumber
     *          level's number
     * @param score
     *          level's score
     */
    void setupLevel(final int levelNumber, final int score);

    /**
     * The loop which manages the gameplay.
     */
    void mainLoop();

    /**
     * Gets survival mode.
     *
     * @return
     *          true if survival.
     */
    boolean isSurvivalMode();
}
