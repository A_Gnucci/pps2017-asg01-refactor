package laterunner.model.world;

import laterunner.model.collisions.BorderBoundingBox;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.saving.FileManager;
import laterunner.model.user.UserStatistics;
import laterunner.model.user.User;
import laterunner.model.vehicle.Obstacle;

/**
 *The class in which is implemented the game's state.
 *
 */
public class GameStateImpl implements GameState {

    private final World world;
    private int score;
    private final int level;
    private boolean isSurvivalModeEnded;

    /**
     * 
     * @param worldEventListener
     *          the listener to be set in World
     * @param level
     *          the level to play
     * @param score
     *          the initial score of the level
     */
    public GameStateImpl(final WorldEventListener worldEventListener, final int level, final int score) {
        this.level = level;
        this.isSurvivalModeEnded = false;
        world = new WorldImpl(BorderBoundingBox.getInstance());
        world.generateLevel(this.level);
        world.setEventListener(worldEventListener);
        this.score = score;
    }

    @Override
    public World getWorld() {
        return this.world;
    }

    @Override
    public void decreaseScore(final Obstacle obstacle) {
        this.score -= obstacle.getMalus();
        if (this.score < 0) {
            this.score = 0;
        }
        User.getInstance().setUserLives(User.getInstance().getUserLives() - obstacle.getLifeDamage());
        UserStatistics userStatistics = User.getInstance().getStatistics();
        userStatistics.setLostLives(userStatistics.getLostLives() + obstacle.getLifeDamage());
        switch(obstacle.getType()) {
            case OBSTACLE_CAR:
                userStatistics.setCarHits(userStatistics.getCarHits() + 1);
                break;
            case MOTORBIKE:
                userStatistics.setMotorbikeHits(userStatistics.getMotorbikeHits() + 1);
                break;
            case BUS:
                userStatistics.setTruckHits(userStatistics.getTruckHits() + 1);
                break;
            default:
                throw new IllegalStateException();
        }
      }

    @Override
    public void decreaseScorebyBorder() {
        this.score -= world.getBorderDamage();
        if (this.score < 0) {
            this.score = 0;
        }
    }

    @Override
    public void increaseScore(final int amount) {
        this.score += amount;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public void update(final int elapsed) {
        this.world.updateState(elapsed);
    }

    @Override
    public boolean isLevelFinished() {
        return this.getWorld().getSceneEntities().size() == 1 || User.getInstance().getUserLives() <= 0;
    }

    @Override
    public boolean isSurvivalModeEnded() {
        return isSurvivalModeEnded;
    }

    @Override
    public void setSurvivalEnded(final boolean survivalEnded) {
        this.isSurvivalModeEnded = survivalEnded;
    }

    @Override
    public void updateUserStatistics() {
        UserStatistics userStatistics = User.getInstance().getStatistics();
        if (this.isSurvivalModeEnded) {
            if (userStatistics.getSurvivalScore() < this.score) {
                userStatistics.setSurvivalScore(this.score);
            }
        } else {
            if (User.getInstance().getUserLives() <= 0) {
                User.getInstance().reset();
            } else {
                User.getInstance().setMoney(User.getInstance().getMoney() + this.score * this.level);
                if (this.level == User.getInstance().getLevelReached()) {
                    User.getInstance().setLevelReached(this.level + 1);
                }
            }
        }
        userStatistics.setGamesPlayed(userStatistics.getGamesPlayed() + 1);
        FileManager.saveToFile();
        FileManager.loadFromFile();
    }
}