package laterunner.input;

import laterunner.model.user.User;
import laterunner.model.world.GameState;
import laterunner.physics.Speed2DImpl;

/**
 * Move left command class.
 */
public class MoveLeft implements Command {

    private static final int SPEED = -300;

    /**
     * Moves the car to the left.
     * 
     * @param gameState
     *          game coordinator
     */
    public void execute(final GameState gameState) {
        gameState.getWorld().getPlayerCar()
                .setSpeed(new Speed2DImpl(SPEED * User.getInstance().getSpeedMultiplier(), 0));
    }

}
