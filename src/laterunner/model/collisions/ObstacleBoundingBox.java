package laterunner.model.collisions;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleType;

/**
 * Horizontal segment of the obstacles' occupied space.
 */
public class ObstacleBoundingBox implements BoundingBox {

    private final double xPosition;
    private final double borderXPosition;

    /**
     * Instantiates the horizontal segment of the obstacle's occupied space.
     * 
     * @param posX
     *          left side of the obstacle
     * @param obstacle
     *          type of the obstacle
     */
    public ObstacleBoundingBox(final double posX, final VehicleType obstacle) {
        this.xPosition = posX;
        this.borderXPosition = this.xPosition + Dimensions.getInstance().getVehicleWidth(obstacle);
    }

    @Override
    public boolean isCollidingWith(final Vehicle car) {
        return car.getPosition().getX() <= borderXPosition &&
                car.getPosition().getX() + Dimensions.getInstance().getVehicleWidth(VehicleType.USER_CAR) >=
                        this.xPosition;
    }
}
