package laterunner.core;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import laterunner.model.collisions.BorderHitEvent;
import laterunner.model.collisions.ObstacleHitEvent;
import laterunner.model.collisions.WorldEvent;
import laterunner.model.saving.FileManager;
import laterunner.model.world.GameState;
import laterunner.model.world.GameStateImplBuilder;
import laterunner.model.world.World;
import laterunner.graphics.panel.MenuPanel;
import laterunner.graphics.Scene;
import laterunner.graphics.SceneImpl;
import laterunner.input.Command;

/**
 * Game's engine.
 */
public class GameEngineImpl implements GameEngine {

    private static final long FRAME_DURATION = 16; /* 16 ms = 60 fps */
    private GameState gameState;
    private Scene view;
    private boolean playerCommandsEnabled = false;
    private final BlockingQueue<Command> commands;
    private final List<WorldEvent> eventQueue;
    private int levelNumber;
    private boolean survivalMode = false;

    /**
     * Instantiates game engine.
     */
    public GameEngineImpl() {
        commands = new ArrayBlockingQueue<>(100);
        eventQueue = new LinkedList<>();
    }

    /**
     * Loads main settings and shows main menu.
     */
    public void initialize() {
        FileManager.loadFromFile();
        view = new SceneImpl(this, this);
    }

    /**
     * Loads level.
     * 
     * @param levelNumber
     *          level's number
     * @param score
     *          level's score
     */
    public void setupLevel(final int levelNumber, final int score) {
        this.commands.clear();
        this.levelNumber = levelNumber;
        if(levelNumber>10){
            if(this.survivalMode){
                this.levelNumber = 10;
            } else{
                this.survivalMode = true;
                this.levelNumber = 1;
            }
        }
        this.gameState = new GameStateImplBuilder()
                .setWorldEventListener(this)
                .setLevel(this.levelNumber)
                .setScore(score)
                .build();
        this.view.getRoad().setGameState(this.gameState);
    }

    /**
     * The loop which manages the gameplay.
     */
    public void mainLoop() {
        this.playerCommandsEnabled = true;
        view.getRoad().getAudio().play();
        long lastTime = System.currentTimeMillis();
        while (!gameState.isLevelFinished() && !this.gameState.isSurvivalModeEnded()) {
            long current = System.currentTimeMillis();
            int elapsed = (int) (current - lastTime);
            processInput();
            updateGame(elapsed);
            view.render();
            waitForNextFrame(current);
            lastTime = current;
        }
        this.gameState.updateUserStatistics();
        endLevel();
    }

    /**
     * Makes the main loop sleeping.
     *
     * @param current
     *          time used to calculate how much to waits
     */
    private void waitForNextFrame(final long current) {
        long timeElapsed = System.currentTimeMillis() - current;
        if (timeElapsed < FRAME_DURATION) {
            try {
                Thread.sleep(FRAME_DURATION - timeElapsed);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void processInput() {
        Command command = commands.poll();
        if (command != null) {
            command.execute(this.gameState);
        }
    }

    private void updateGame(final int elapsed) {
        this.gameState.update(elapsed);
        checkEvents();
    }

    private void checkEvents() {
        World world = gameState.getWorld();
        eventQueue.forEach(event -> {
            if (event instanceof ObstacleHitEvent) {
                ObstacleHitEvent hit = (ObstacleHitEvent) event;
                if (this.survivalMode) {
                    this.gameState.setSurvivalEnded(true);
                    this.survivalMode = false;
                } else {
                    world.removeObstacle(hit.getObstacle());
                    gameState.decreaseScore(hit.getObstacle());
                }
            } else if (event instanceof BorderHitEvent) {
                gameState.decreaseScorebyBorder();
            }
        });
        if (eventQueue.isEmpty()) {
            gameState.increaseScore(2);
        }
        eventQueue.clear();
    }

    private void endLevel() {
        if (!this.survivalMode) {
            this.playerCommandsEnabled = false;
            view.getRoad().getAudio().stop();
            SceneImpl.changePanel("menu");
            MenuPanel.updateLevel();
        } else {
            this.setupLevel(++this.levelNumber, this.gameState.getScore());
            new Thread(this.view.getRoad()).start();
        }
    }

    /**
     * Adds the command to the command queue.
     * 
     * @param command
     *          Command executed
     */
    @Override
    public void notifyCommand(final Command command) {
        if (this.playerCommandsEnabled) {
            commands.add(command);
        }
    }

    /**
     * Adds the event to the event queue.
     * 
     * @param event
     *          Event happened
     */
    @Override
    public void notifyEvent(final WorldEvent event) {
        eventQueue.add(event);
    }

    /**
     * Gets survival mode.
     * 
     * @return
     *          true if survival.
     */
    public boolean isSurvivalMode() {
        return survivalMode;
    }
}