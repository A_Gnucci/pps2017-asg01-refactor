package laterunner.model.user;

import laterunner.model.shop.Shop;

/**
 * Game user.
 */
public final class User {

    private static final int INITIAL_USER_LIFE = 3;
    private static final int STARTING_MONEY = 1000;
    private static final int STARTING_LEVEL = 1;
    private static final double STARTING_SPEED_MULTIPLIER = 1.0;
    private static final User instance = new User();

    private int money;
    private int levelReached;
    private double speedMultiplier;
    private int userLives;
    private final UserStatistics statistics;

    private User() {
        this.money = STARTING_MONEY;
        this.levelReached = STARTING_LEVEL;
        this.speedMultiplier = STARTING_SPEED_MULTIPLIER;
        this.userLives = INITIAL_USER_LIFE;
        this.statistics = new UserStatisticsImpl();
    }

    /**
     * Returns the only class' instance.
     * 
     * @return
     *          user's instance
     */
    public static User getInstance() {
        return instance;
    }

    /**
     * Returns user's money.
     * 
     * @return
     *          user's money
     */
    public int getMoney() {
        return this.money;
    }

    /**
     * Sets user's money amount.
     * 
     * @param amount
     *          new user's money amount
     */
    public void setMoney(final int amount) {
        assert amount >= 0;
        this.money = amount;
    }

    /**
     * Returns user's level reached.
     * 
     * @return
     *          user's level reached
     */
    public int getLevelReached() {
        return this.levelReached;
    }

    /**
     * Sets user's level reached.
     * 
     * @param lvlReached
     *          new user's level reached
     */
    public void setLevelReached(final int lvlReached) {
        assert lvlReached >= 1;
        this.levelReached = lvlReached;
    }

    /**
     * Returns user's multiplier.
     * 
     * @return
     *          user's speed multiplier
     */
    public double getSpeedMultiplier() {
        return this.speedMultiplier;
    }

    /**
     * Sets user's speed multiplier.
     * 
     * @param newMul
     *          new user's speed multiplier(1.0, 1.5 or 2.0)
     */
    public void setSpeedMultiplier(final double newMul) {
        assert newMul == STARTING_SPEED_MULTIPLIER || newMul == STARTING_SPEED_MULTIPLIER + 0.5
                || newMul == STARTING_SPEED_MULTIPLIER + 1.0;
        this.speedMultiplier = newMul;
    }

    /**
     * Returns user's lives.
     * 
     * @return
     *          user's lives
     */
    public int getUserLives() {
        return this.userLives;
    }

    /**
     * Sets user's lives.
     * 
     * @param lives
     *          new user's lives
     */
    public void setUserLives(final int lives) {
        if (lives < 0) {
            this.userLives = 0;
        } else {
            this.userLives = lives;
        }
    }

    /**
     * Resets user's fields and shop's references.
     */
    public void reset() {
        this.money = STARTING_MONEY;
        this.levelReached = STARTING_LEVEL;
        this.speedMultiplier = STARTING_SPEED_MULTIPLIER;
        this.userLives = INITIAL_USER_LIFE;
        Shop.getInstance().reset();
    }

    /**
     * Gets the statistics of this user
     * */
    public UserStatistics getStatistics(){
        return statistics;
    }
}