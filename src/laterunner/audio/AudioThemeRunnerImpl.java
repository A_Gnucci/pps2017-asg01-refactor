package laterunner.audio;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * AudioThemeRunnerImpl is an utility class that provides methods to play music during the game.
 *
 */
public class AudioThemeRunnerImpl implements AudioThemeRunner{

    private Clip clip;

/**
 * Common AudioThemeRunnerImpl constructor.
 */
   public AudioThemeRunnerImpl() {
      try {
         URL url = this.getClass().getClassLoader().getResource("LateRunner_Theme.wav");
         Objects.requireNonNull(url);
         AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
         clip = AudioSystem.getClip();
         clip.open(audioIn);
      } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
         e.printStackTrace();
      }
   }

   /**
    * Method to play audio.
    */
   public void play() {
       clip.loop(Clip.LOOP_CONTINUOUSLY);
   }

   /**
    * Method to stop audio.
    */
   public void stop() {
       clip.stop();
       clip.setFramePosition(0);
   }

}
