package laterunner.model.user;

/**
 * User's statistics.
 */
public final class UserStatisticsImpl implements UserStatistics {

    private long motorbikeHits = 0;
    private long carHits = 0;
    private long truckHits = 0;
    private long gamesPlayed = 0;
    private long lostLives = 0;
    private long survivalScore = 0;

    /**
     * Returns motorbike hits' number.
     *
     * @return
     *          motorbike hits' number
     */
    public long getMotorbikeHits() {
        return this.motorbikeHits;
    }

    /**
     * Sets new motorbike hits' number.
     *
     * @param bikeHits
     *          new motorbike hits' number
     */
    public void setMotorbikeHits(final long bikeHits) {
        assert bikeHits >= this.motorbikeHits;
        this.motorbikeHits = bikeHits;
    }

    /**
     * Returns obstacle car hits' number.
     *
     * @return
     *          obstacle car hits' number
     */
    public long getCarHits() {
        return this.carHits;
    }

    /**
     * Sets new obstacle car hits' number.
     *
     * @param carHits
     *          new obstacle car hits' number
     */
    public void setCarHits(final long carHits) {
        assert carHits >= this.carHits;
        this.carHits = carHits;
    }

    /**
     * Returns truck hits' number.
     *
     * @return
     *          truck hits' number
     */
    public long getTruckHits() {
        return this.truckHits;
    }

    /**
     * Sets new truck hits' number.
     *
     * @param truckHits
     *          new truck hits' number
     */
    public void setTruckHits(final long truckHits) {
        assert truckHits >= this.truckHits;
        this.truckHits = truckHits;
    }

    /**
     * Returns games played number.
     *
     * @return
     *          games played number
     */
    public long getGamesPlayed() {
        return this.gamesPlayed;
    }

    /**
     * Sets new games played number.
     *
     * @param gamesPlayed
     *          new games played number
     */
    public void setGamesPlayed(final long gamesPlayed) {
        assert gamesPlayed >= this.gamesPlayed;
        this.gamesPlayed = gamesPlayed;
    }

    /**
     * Returns lost lives' number.
     *
     * @return
     *          lost lives' number
     */
    public long getLostLives() {
        return this.lostLives;
    }

    /**
     * Sets lost lives' number.
     *
     * @param lstLives
     *          new lost lives' number
     */
    public void setLostLives(final long lstLives) {
        assert lstLives >= this.lostLives;
        this.lostLives = lstLives;
    }

    /**
     * Returns survival score.
     *
     * @return
     *          survival score number
     */
    public long getSurvivalScore() {
        return this.survivalScore;
    }

    /**
     * Sets survival score number.
     *
     * @param score
     *          new survival score number
     */
    public void setSurvivalScore(final long score) {
        assert score >= this.survivalScore;
        this.survivalScore = score;
    }
}