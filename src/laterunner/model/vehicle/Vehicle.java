package laterunner.model.vehicle;

import laterunner.physics.Position2D;
import laterunner.physics.Speed2D;

public interface Vehicle {

    /**
     * Returns vehicle's current position.
     *
     * @return
     *          vehicle's current position
     */
    Position2D getPosition();

    /**
     * Sets vehicle's position.
     *
     * @param position
     *          vehicle's new position
     */
    void setPosition(final Position2D position);

    /**
     * Returns vehicle's current speed.
     *
     * @return
     *          vehicle's current speed
     */
    Speed2D getSpeed();

    /**
     * Recalls the checking setter.
     *
     * @param speed
     *          unchecked vehicle's new speed
     */
    void setSpeed(final Speed2D speed);

    /**
     * Calculates new vehicle's position depending on the speed.
     *
     * @param elapsed
     *          time elapsed between two frames
     */
    void updateState(final int elapsed);
}
