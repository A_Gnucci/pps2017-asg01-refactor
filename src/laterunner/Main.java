package laterunner;

import laterunner.core.GameEngineImpl;

/**
 * Main class.
 */
final class Main {

    private Main() { }

    /**
     * Main method.
     * 
     * @param args
     *          args
     */
    public static void main(final String[] args) {
        new GameEngineImpl().initialize();
    }

}
