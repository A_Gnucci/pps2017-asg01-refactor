package laterunner.graphics;

/**
 * Icon defines all the graphics resources.
 *
 */
public enum Icon {

    /**
     * Back picture.
     */
    BACK,

    /**
     * Background picture.
     */
    BACKGROUND,

    /**
     * Bus picture.
     */
    BUS,

    /**
     * Buy_Life picture.
     */
    BUY_LIFE,

    /**
     * Buy_Speed picture.
     */
    BUY_SPEED,

    /**
     * PlayerCar picture.
     */
    CAR,

    /**
     * Coin picture.
     */
    COIN,

    /**
     * Cross picture.
     */
    CROSS,

    /**
     * Heart picture.
     */
    HEART,

    /**
     * Jeep picture.
     */
    JEEP,

    /**
     * MenuPanel picture.
     */
    MENU,

    /**
     * Motor-bike picture.
     */
    MOTORBIKE,

    /**
     * Play picture.
     */
    PLAY,

    /**
     * Quit picture.
     */
    QUIT,

    /**
     * RoadPanel picture.
     */
    ROAD,

    /**
     * Shop picture.
     */
    SHOP,

    /**
     * StatisticsPanel picture.
     */
    STATS

}
