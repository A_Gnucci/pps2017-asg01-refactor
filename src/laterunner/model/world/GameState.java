package laterunner.model.world;

import laterunner.model.vehicle.Obstacle;

/**
 * The interface in which is defined the game's state.
 *
 */
public interface GameState {

    /**
     * Return the GameState's world.
     * 
     * @return
     *          the GameState's world
     */
    World getWorld();

    /**
     * Decrease the score by the malus of obstacle.
     * 
     * @param obstacle
     *          the ObstacleImpl hit
     */
    void decreaseScore(final Obstacle obstacle);

    /**
     * Decrease the score by the malus of the border.
     * 
     */
    void decreaseScorebyBorder();

    /**
     * Increase score by amount.
     * 
     * @param amount
     *          the value to increase the score
     */
    void increaseScore(final int amount);

    /**
     * Get the level's score.
     * 
     * @return
     *          the score
     */
    int getScore();

    /**
     * Update the world by elapsed.
     * 
     * @param elapsed
     *          the time elapsed
     */
    void update(final int elapsed);

    /**
     * Check if the level is finished.
     * 
     * @return
     *              true is the level is finished
     */
    boolean isLevelFinished();

    /**
     *  Return true is mood survival is finished.
     * 
     * @return
     *          true is mood survival is finished
     */
    boolean isSurvivalModeEnded();

    /**
     * Set the mood survival at endSurvival.
     * 
     * @param survivalEnded
     *          the parameter to set survival
     */
    void setSurvivalEnded(final boolean survivalEnded);

    /**
     * Update the User statistics.
     */
    void updateUserStatistics();
}