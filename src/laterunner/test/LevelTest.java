package laterunner.test;

import java.util.List;

import laterunner.model.level.LevelManager;
import laterunner.model.vehicle.Obstacle;
import org.junit.BeforeClass;
import org.junit.Test;

import laterunner.model.level.LevelManagerImpl;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test the creation of level.
 *
 */
public class LevelTest {

    private static List<Obstacle> level1Obstacles;
    private static List<Obstacle> level10Obstacles;

    @BeforeClass
    public static void init(){
        final LevelManager levelSetter = new LevelManagerImpl();
        level1Obstacles = levelSetter.getLevel(1).getObstacles();
        level10Obstacles = levelSetter.getLevel(10).getObstacles();
    }

    @Test
    public void testObstaclesPosition() {
        testObstaclesPosition(level1Obstacles);
        testObstaclesPosition(level10Obstacles);
    }

    private void testObstaclesPosition(List<Obstacle> obstacles){
        obstacles.stream()
                .filter(obstacle -> !obstacles.get(0).equals(obstacle))
                .forEach(obstacle -> testObstaclePosition(obstacle, obstacles.get(obstacles.indexOf(obstacle) - 1)));
    }

    private void testObstaclePosition(Obstacle obstacle, Obstacle previousObstacle){
        final int MIN_DISTANCE = 150;
        if (obstacle.getPosition().getY() != previousObstacle.getPosition().getY()) {
            assertTrue((obstacle.getPosition().getY() - previousObstacle.getPosition().getY()) >=
                    MIN_DISTANCE);
        } else {
            assertNotEquals(obstacle.getPosition().getX(), previousObstacle.getPosition().getX(), 0.0);
        }
    }

    @Test
    public void testObstaclesSpeed(){
        testObstaclesSpeed(level1Obstacles, 300);
        testObstaclesSpeed(level10Obstacles, 800);

    }

    private void testObstaclesSpeed(List<Obstacle> obstacles, int obstacleSpeed){
        obstacles.forEach(obstacle -> assertEquals(obstacleSpeed, obstacle.getSpeed().getYSpeed(), 0.0));
    }

    @Test
    public void testObstaclesCountInLevels(){
        final LevelManager levelManager = new LevelManagerImpl();
        assertEquals(50, levelManager.getLevel(1).getObstacles().size());
        assertEquals(200, levelManager.getLevel(10).getObstacles().size());
    }
}