package laterunner.physics;

public interface Position2D extends java.io.Serializable {

    /**
     * Translate the point.
     *
     * @param vector
     *          the vector to translate the point
     * @return
     *          the point after the translation
     */
    Position2D sum(final Speed2D vector);

    /**
     * Get the x component.
     *
     * @return
     *          the x component
     */
    double getX();

    /**
     * Set the x component.
     *
     * @param x
     *          the new x component
     */
    void setX(final double x);

    /**
     * Get the y component.
     *
     * @return
     *          the y component
     */
    double getY();

    /**
     * Set the y component.
     *
     * @param y
     *          the new y component
     */
    void setY(final double y);
}
