package laterunner.model.vehicle;

import laterunner.physics.Position2D;
import laterunner.physics.Speed2D;

public class VehicleFactoryImpl implements VehicleFactory {

    @Override
    public Vehicle createPlayerCar() {
        return new PlayerCar();
    }

    @Override
    public Obstacle createObstacleCar(final Position2D position, final Speed2D speed) {
        return new ObstacleImpl(VehicleType.OBSTACLE_CAR, position, speed);
    }

    @Override
    public Obstacle createObstacleBike(final Position2D position, final Speed2D speed) {
        return new ObstacleImpl(VehicleType.MOTORBIKE, position, speed);
    }

    @Override
    public Obstacle createObstacleBus(final Position2D position, final Speed2D speed) {
        return new ObstacleImpl(VehicleType.BUS, position, speed);
    }

    public Obstacle createObstacleByType(VehicleType type, Position2D position, Speed2D speed){
        VehicleFactory vehicleFactory = new VehicleFactoryImpl();
        switch (type){
            case BUS: return vehicleFactory.createObstacleBus(position, speed);
            case OBSTACLE_CAR: return vehicleFactory.createObstacleCar(position, speed);
            case MOTORBIKE: return vehicleFactory.createObstacleBike(position, speed);
            default: throw new IllegalArgumentException("Cannot create user car as an obstacle");
        }
    }
}
