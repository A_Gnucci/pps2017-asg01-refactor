package laterunner.model.collisions;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Position2D;
import laterunner.physics.Position2DImpl;

/**
 * Square in which the car and obstacles can appear.
 */
public final class BorderBoundingBox implements BoundingBox {

    private static final Position2D UP_LEFT = new Position2DImpl(172, 0);
    private static final Position2D DOWN_RIGHT = new Position2DImpl(773, 511);
    private static BorderBoundingBox instance;

    /**
     * Istantiates the street bounding box.
     */
    private BorderBoundingBox() { }

    /**
     * Returns the only class' instance.
     * 
     * @return
     *          border bounding box
     */
    public static BorderBoundingBox getInstance() {
        if (instance == null) {
            instance = new BorderBoundingBox();
        }
        return instance;
    }

    @Override
    public boolean isCollidingWith(final Vehicle car) {
        boolean collision = false;
        if (car.getPosition().getX() + Dimensions.getInstance().getVehicleWidth(VehicleType.USER_CAR) >
                DOWN_RIGHT.getX()) {
            car.setPosition(new Position2DImpl(DOWN_RIGHT.getX() - Dimensions.getInstance().getVehicleWidth(VehicleType.USER_CAR) - 1,
                    car.getPosition().getY()));
            collision = true;
        } else if (car.getPosition().getX() < UP_LEFT.getX()) {
            car.setPosition(new Position2DImpl(UP_LEFT.getX() + 1, car.getPosition().getY()));
            collision = true;
        }
        return collision;
    }

    /**
     * Returns box' upper left corner.
     * 
     * @return
     *          box' upper left corner.
     */
    public Position2D getUpperLeft() {
        return UP_LEFT;
    }

    /**
     * Returns box' below right corner.
     * 
     * @return
     *          box' below right corner.
     */
    public Position2D getBelowRight() {
        return DOWN_RIGHT;
    }
}
