package laterunner.graphics.panel;

import laterunner.graphics.Icon;
import laterunner.graphics.SceneImpl;
import laterunner.model.user.User;
import laterunner.model.user.UserStatistics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *StatisticsPanel class displays all the info about user's game experience.
 *
 */
public class StatisticsPanel extends PanelImpl {

    private static final float FONT_SIZE = 60;
    private static final JLabel label = new JLabel();

    /**
     * Common StatisticsPanel constructor: sets the panel's layout and draws the components over the background.
     */
    public StatisticsPanel() {
        this.setLayout(new BorderLayout());

        JButton backButton = super.createButton(super.getPics().getIcon(Icon.BACK));
        backButton.addActionListener(e -> SceneImpl.changePanel("menu"));

        label.setFont(super.createFont(FONT_SIZE));
        label.setForeground(Color.WHITE);

        JPanel panel = new JPanel();
        panel.setOpaque(false);
        panel.add(label);
        this.add(panel, BorderLayout.CENTER);
        this.add(backButton, BorderLayout.PAGE_END);
    }

    @Override
    protected void paintComponent(final Graphics g) {
        g.drawImage(super.getPics().getImage(Icon.BACKGROUND), 0, 0, null);
        UserStatistics userStatistics = User.getInstance().getStatistics();
        String text = "<br>Games Played: " + userStatistics.getGamesPlayed() + "<br>"
                + "Lives Lost: " + userStatistics.getLostLives() + "<br>"
                + "Survival Highscore: " + userStatistics.getSurvivalScore() + "<br>"
                + "Motorbike Hits: " + userStatistics.getMotorbikeHits() + "<br>"
                + "Car Hits: " + userStatistics.getCarHits() + "<br>"
                + "Bus Hits: " + userStatistics.getTruckHits();
        label.setText("<html><div style='text-align: center;'>" + text + "</div></html>");
    }
}

