package laterunner.model.level;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import laterunner.core.Dimensions;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.VehicleFactory;
import laterunner.model.vehicle.VehicleFactoryImpl;
import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Position2D;
import laterunner.physics.Position2DImpl;
import laterunner.physics.Speed2D;

/**
 * The class in witch is implemented the method of levels' creation.
 *
 */
public class LevelGeneratorImpl implements LevelGenerator {

    private static final int DISTANCE = 150;
    private static final int WIDTH_ROAD = 100;
    private static final int WIDTH_ROAD_PARALLEL =
            (int) (WIDTH_ROAD - Dimensions.getInstance().getVehicleWidth(VehicleType.BUS));
    private static final int MOTOBIKE_GAP = 85;
    private static final int BUS_FRONT_GAP = 10;
    private static final int BUS_BACK_GAP = 83;
    private static final int CAR_FRONT_GAP = 55;
    private static final int CAR_BACK_GAP = 27;
    private static final int INITIAL_GAP = -400;
    private static final double QUEUE_RATE = 0.3;
    private static final double PARALLEL_RATE = 0.5;
    private static final int LEFT = 220;
    private static final int CENTER = 410;
    private static final int RIGHT = 600;
    private double gap = INITIAL_GAP;
    private double cont = 0;

    @Override
    public Level generateLevel(final List<VehicleType> vehicles, final Speed2D speed, final int distance) {
        List<Obstacle> obstacles = new LinkedList<>();
        int vehiclesCount = vehicles.size();
        VehicleFactory vehicleFactory = new VehicleFactoryImpl();
        IntStream.range(0, vehiclesCount).map(i -> vehiclesCount -i).forEach(index -> {
            int vehicleIndex = (int) (Math.random() * index);
            if (obstacles.isEmpty()) {
                obstacles.add(vehicleFactory.createObstacleByType(vehicles.get(vehicleIndex),
                        new Position2DImpl(getRandomPosition(), 0), speed));
            } else {
                Position2D pos = new Position2DImpl(getRandomPosition(), obstacles.get(obstacles.size() - 1).getPosition().getY() +
                        getYPosition(distance, vehicles.get(vehicleIndex), obstacles));
                if (obstacles.size() >= 3) {
                    changePosition(obstacles, pos);
                }
                cont = cont + (pos.getY() - obstacles.get(obstacles.size() - 1).getPosition().getY());
                obstacles.add(vehicleFactory.createObstacleByType(vehicles.get(vehicleIndex), pos, speed));
            }
            vehicles.remove(vehicleIndex);
        });
        gap = gap - cont;

        obstacles.forEach(o -> o.getPosition().setY(o.getPosition().getY() + gap));
        Level level = new LevelImpl();
        level.setObstacles(obstacles);
        return level;
    }

    private int getRandomPosition() {
        switch(new Random().nextInt(3) + 1) {
            case 1:
                return new Random().nextInt(WIDTH_ROAD) + LEFT;
            case 2:
                return new Random().nextInt(WIDTH_ROAD) + CENTER;
            default: return new Random().nextInt(WIDTH_ROAD) + RIGHT;
        }
    }

    private int getYPosition(final int maximumRandomDistance, final VehicleType vehicleType,
                             final List<Obstacle> obstacles) {
        int distance = (int) (new Random().nextInt(maximumRandomDistance) +
                ((LevelGeneratorImpl.DISTANCE) +  Dimensions.getInstance().getVehicleHeight(vehicleType)));

        if (vehicleType == VehicleType.MOTORBIKE) {
            distance += MOTOBIKE_GAP;
        } else if (vehicleType == VehicleType.BUS) {
            distance += BUS_FRONT_GAP;
        } else if (vehicleType == VehicleType.OBSTACLE_CAR) {
            distance += CAR_FRONT_GAP;
        }
        VehicleType lastVehicle = obstacles.get(obstacles.size() - 1).getType();
        if (lastVehicle == VehicleType.BUS) {
            distance += BUS_BACK_GAP;
        } else if (lastVehicle == VehicleType.OBSTACLE_CAR) {
            distance += CAR_BACK_GAP;
        }
        return distance;
    }

    private void changePosition(final List<Obstacle> obstacles, final Position2D position) {
        if (this.isInTheSameRange(obstacles, position, 1) && new Random().nextDouble() > QUEUE_RATE) {
            double x = this.getRange(obstacles.get(obstacles.size() - 2).getPosition());
            if (this.isInTheSameRange(obstacles, position, 2)) {
                position.setX(this.getRandomPosition());
            } else if (x == LEFT && this.getRange(position) == CENTER || x == CENTER && this.getRange(position) == LEFT) {
                position.setX(new Random().nextInt(WIDTH_ROAD) + RIGHT);
            } else if (x == LEFT && this.getRange(position) == RIGHT || x == RIGHT && this.getRange(position) == LEFT) {
                position.setX(new Random().nextInt(WIDTH_ROAD) + CENTER);
            } else {
                position.setX(new Random().nextInt(WIDTH_ROAD) + LEFT);
            }
        }

        setPosition(obstacles, position);
        checkDuplicateObstacles(obstacles, position);
    }

    private boolean isInTheSameRange(final List<Obstacle> list, final Position2D pos, final int x) {
        return (this.getRange(list.get(list.size() - x).getPosition()) == this.getRange(pos));
    }

    private void setPosition(final List<Obstacle> obstacles, final Position2D position){
        if (this.checkEmptyLine(obstacles, position, LEFT, RIGHT)) {
            this.setParallelInCenter(obstacles, position, RIGHT, CENTER);
            this.checkDuplicateObstacle(obstacles, position);
        }
        if (this.checkEmptyLine(obstacles, position, RIGHT, LEFT)) {
            this.setParallelInCenter(obstacles, position, LEFT, CENTER);
        }
        if (this.checkEmptyLine(obstacles, position, LEFT, CENTER)) {
            position.setX(new Random().nextInt(WIDTH_ROAD) + RIGHT);
        }
        if (this.checkEmptyLine(obstacles, position, RIGHT, CENTER)) {
            position.setX(new Random().nextInt(WIDTH_ROAD) + LEFT);
        }
        if (this.checkEmptyLine(obstacles, position, CENTER, LEFT)) {
            position.setX(new Random().nextInt(WIDTH_ROAD) + RIGHT);
        }
        if (this.checkEmptyLine(obstacles, position, CENTER, RIGHT)) {
            position.setX(new Random().nextInt(WIDTH_ROAD) + LEFT);
        }
    }

    private boolean checkEmptyLine(final List<Obstacle> obstacles, final Position2D position, final int first,
                                   final int second) {
        return this.getRange(position) == first && this.checkPos(obstacles, 1, second) &&
                this.checkPos(obstacles, 2, second) && this.checkPos(obstacles, 3, first);
    }

    private int getRange(final Position2D position) {
        if (position.getX() >= LEFT && position.getX() <= WIDTH_ROAD + LEFT) {
            return LEFT;
        } else if (position.getX() >= CENTER && position.getX() <= WIDTH_ROAD + CENTER) {
            return CENTER;
        } else {
            return RIGHT;
        }
    }

    private void checkDuplicateObstacles(final List<Obstacle> obstacles, final Position2D position){
        if (this.checkTrianglePosition(obstacles, position, LEFT, RIGHT)) {
            this.setParallelInSide(obstacles, position, LEFT);
        }
        if (this.checkTrianglePosition(obstacles, position, RIGHT, LEFT)) {
            this.setParallelInSide(obstacles, position, RIGHT);
            this.checkDuplicateObstacle(obstacles, position);
        }
        if (this.checkTrianglePosition(obstacles, position, LEFT, CENTER) && new Random().nextDouble() > PARALLEL_RATE) {
            this.setParallelInCenter(obstacles, position, CENTER, LEFT);
            this.checkDuplicateObstacle(obstacles, position);
        }
        if (this.checkTrianglePosition(obstacles, position, RIGHT, CENTER) && new Random().nextDouble() > PARALLEL_RATE) {
            this.setParallelInCenter(obstacles, position, CENTER, RIGHT);
            this.checkDuplicateObstacle(obstacles, position);
        }
        if (this.checkEmptyCenterLine(obstacles, RIGHT)) {
            this.setParallelInCenter(obstacles, position, RIGHT, CENTER);
            this.checkDuplicateObstacle(obstacles, position);
        }
        if (this.checkEmptyCenterLine(obstacles, LEFT)) {
            this.setParallelInCenter(obstacles, position, LEFT, CENTER);
            this.checkDuplicateObstacle(obstacles, position);
        }
    }

    private boolean checkPos(final List<Obstacle> obstacles, final int x, final int range) {
        return this.getRange(obstacles.get(obstacles.size() - x).getPosition()) == range;
    }

    private void setParallelInCenter(final List<Obstacle> obstacles, final Position2D position, final int first,
                                     final int second) {
        obstacles.get(obstacles.size() - 1).getPosition().setX(new Random().nextInt(WIDTH_ROAD_PARALLEL) + first);
        position.setX(new Random().nextInt(WIDTH_ROAD_PARALLEL) + second);
        position.setY(obstacles.get(obstacles.size() - 1).getPosition().getY());
    }

    private void checkDuplicateObstacle(final List<Obstacle> obstacles, final Position2D position) {
        if (position.getY() == obstacles.get(obstacles.size() - 1).getPosition().getY() &&
                this.isInTheSameRange(obstacles, position, 1)) {
            position.setY(position.getY() + LevelGeneratorImpl.DISTANCE);
        }
    }

    private boolean checkTrianglePosition(final List<Obstacle> obstacles, final Position2D position, final int first,
                                          final int second) {
        return this.getRange(position) == first
                && this.checkPos(obstacles, 1, second)
                && this.checkPos(obstacles, 2, first)
                && this.getRange(obstacles.get(obstacles.size() - 3).getPosition()) != second
                && !(obstacles.get(obstacles.size() - 1).getPosition().getY() ==
                obstacles.get(obstacles.size() - 2).getPosition().getY());
    }

    private void setParallelInSide(final List<Obstacle> obstacles, final Position2D position, final int side) {
        position.setX(new Random().nextInt(WIDTH_ROAD) + side);
        position.setY(obstacles.get(obstacles.size() - 1).getPosition().getY());
    }

    private boolean checkEmptyCenterLine(final List<Obstacle> obstacles, final int side) {
        return this.checkPos(obstacles, 1, side) && this.checkPos(obstacles, 2, side)
                && this.getRange(obstacles.get(obstacles.size() - 3).getPosition()) != CENTER;
    }
}