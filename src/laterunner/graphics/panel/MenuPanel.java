package laterunner.graphics.panel;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import laterunner.core.GameEngine;
import laterunner.graphics.Icon;
import laterunner.graphics.SceneImpl;
import laterunner.model.user.User;

/**
 * MenuPanel is the class that displays the main menu and it's the first panel showed when the game starts.
 */
public class MenuPanel extends PanelImpl {

    private static final int X_BOX_POSITION = 72;
    private static final int Y_BOX_POSITION = 268;
    private static final int SPACE_BETWEEN_PARTS = 10;
    private static final int LEVELS_COUNT = 11;
    private static final float FONT_SIZE = 48;
    private final GameEngine gameEngine;
    private int currentLevel = 1;
    private static int unblockedLevelThreshold;

    /**
     * Common MenuPanel constructor. It has no layout and draws all the components relative to each other.
     *
     * @param road
     *          an instance of RoadPanel class
     * @param gameEngine
     *          an instance of GameEngine
     */
    public MenuPanel(final RoadPanel road, final GameEngine gameEngine) {
        this.setLayout(null);
        Font font = super.createFont(FONT_SIZE);
        this.gameEngine = gameEngine;
        updateLevel();
        setupComboBox(font);
        setupButtons(road);
    }

    private void setupComboBox(Font font){
        JComboBox<String> levelsComboBox = new JComboBox<>();
        levelsComboBox.setFont(font);

        DefaultListCellRenderer listRenderer = new DefaultListCellRenderer();
        listRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        levelsComboBox.setRenderer(listRenderer);

        IntStream.rangeClosed(1, LEVELS_COUNT-1).forEach(n -> levelsComboBox.addItem(Integer.toString(n)));
        levelsComboBox.addItem("S");

        setComboBoxListener(levelsComboBox);

        levelsComboBox.setEditable(true);
        levelsComboBox.setOpaque(false);
        ((JTextField) levelsComboBox.getEditor().getEditorComponent()).setOpaque(false);
        ImageIcon playPng = super.getPics().getIcon(Icon.PLAY);
        levelsComboBox.setBounds(X_BOX_POSITION + playPng.getIconWidth() + SPACE_BETWEEN_PARTS, Y_BOX_POSITION,
                playPng.getIconHeight(), playPng.getIconHeight());
        this.add(levelsComboBox);
    }

    private void setComboBoxListener(JComboBox comboBox){
        comboBox.addItemListener(e -> {
            if (e.getStateChange() != ItemEvent.SELECTED) {
                return;
            }
            if (e.getItemSelectable().getSelectedObjects()[0].equals("S")) {
                currentLevel = LEVELS_COUNT;
            } else {
                currentLevel = Integer.parseInt((String) e.getItemSelectable().getSelectedObjects()[0]);
            }
        });
    }

    private void setupButtons(RoadPanel road){
        JButton playButton = setupPlayButton(road);
        JButton shopButton = setupShopButton(playButton);
        JButton statisticsButton = setupStatisticsButton(shopButton);
        setupQuitButton(statisticsButton);
    }

    private JButton setupPlayButton(RoadPanel road){
        JButton emptyButton = new JButton();
        emptyButton.setSize(0, Y_BOX_POSITION);
        return setupButton(emptyButton, Icon.PLAY, e -> {
            if (currentLevel <= unblockedLevelThreshold || currentLevel == LEVELS_COUNT) {
                this.gameEngine.setupLevel(currentLevel, 0);
                SceneImpl.changePanel("road");
                new Thread(road).start();
            } else {
                JOptionPane.showMessageDialog(this, "You have to unblock it!", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private JButton setupShopButton(JButton playButton){
        return setupButton(playButton, Icon.SHOP, e -> SceneImpl.changePanel("shop"));
    }

    private JButton setupStatisticsButton(JButton shopButton){
        return setupButton(shopButton, Icon.STATS, e -> SceneImpl.changePanel("stats"));
    }

    private void setupQuitButton(JButton statisticsButton){
        setupButton(statisticsButton, Icon.QUIT, e -> System.exit(0));
    }

    private JButton setupButton(JButton previousButton, Icon buttonIcon, Consumer<ActionEvent> action){
        ImageIcon icon = super.getPics().getIcon(buttonIcon);
        JButton button = super.createButton(icon);
        button.setBounds(X_BOX_POSITION, previousButton.getY() + previousButton.getHeight() + SPACE_BETWEEN_PARTS,
                icon.getIconWidth(), icon.getIconHeight());
        button.addActionListener(action::accept);
        this.add(button);
        return button;
    }

    @Override
    protected void paintComponent(final Graphics g) {
        g.drawImage(super.getPics().getImage(Icon.MENU), 0, 0, null);
     }

    /**
     * Updates the number of the last level reached once the player has unlocked it.
     */
    public static void updateLevel() {
        unblockedLevelThreshold = User.getInstance().getLevelReached();
    }
}