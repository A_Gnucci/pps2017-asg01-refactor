package laterunner.audio;

public interface AudioThemeRunner {

    /**
     * Method to play audio.
     */
    void play();

    /**
     * Method to stop audio.
     */
    void stop();

}
