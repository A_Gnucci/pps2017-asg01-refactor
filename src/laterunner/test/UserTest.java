package laterunner.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import laterunner.model.user.User;

/**
 * Tests User's methods.
 */
public class UserTest {

    private static final User user = User.getInstance();

    /**
     * Tests User's initial fields.
     */
    @Test
    public void testInitialFields() {
        user.reset();
        assertEquals(user.getLevelReached(), 1);
        assertEquals(user.getMoney(), 1000);
        assertEquals(1.0, user.getSpeedMultiplier(), 0.0);
        assertEquals(user.getUserLives(), 3);
        user.setUserLives(-1);
        assertEquals(user.getUserLives(), 0);
    }

    /**
     * Tests if methods throw exceptions on particular situations.
     */
    @Test(expected = AssertionError.class)
    public void testLevelReached() {
        user.setLevelReached(-1);
    }

    @Test(expected = AssertionError.class)
    public void testMoneySetter() {
        user.setMoney(-1);
    }

    @Test(expected = AssertionError.class)
    public void testSpeedMultiplierSetter(){
        final double BAD_DOUBLE = 0.9;
        user.setSpeedMultiplier(BAD_DOUBLE);
    }

    @Test(expected = AssertionError.class)
    public void testUserStatistics() {
        User.getInstance().getStatistics().setGamesPlayed(-1);
    }
}
