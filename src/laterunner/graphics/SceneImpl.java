package laterunner.graphics;

import laterunner.core.GameEngine;
import laterunner.graphics.panel.MarketPanel;
import laterunner.graphics.panel.MenuPanel;
import laterunner.graphics.panel.RoadPanel;
import laterunner.graphics.panel.StatisticsPanel;
import laterunner.input.Controller;
import laterunner.input.MoveLeft;
import laterunner.input.MoveRight;
import laterunner.input.Stop;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Scene Implementation.
 *
 */
public class SceneImpl extends JFrame implements Scene, KeyListener {

    private static final long serialVersionUID = 1L;
    private static final int SCENE_WIDTH = 960;
    private static final int SCENE_HEIGHT = 720;
    private static final CardLayout CARD_LAYOUT = new CardLayout();
    private static final JPanel MAIN_PANEL = new JPanel(CARD_LAYOUT);
    private final RoadPanel road;
    private final Controller controller;

    /**
     * Common Scene Constructor: instantiates all the panels and set MenuPanel as first view.
     * 
     * @param gameEngine
     *          instance of Controller class GameEngineImpl
     * @param controller
     *          instance of Controller class Controller
     */
    public SceneImpl(final GameEngine gameEngine, final Controller controller) {
        this.addKeyListener(this);
        this.controller = controller;

        road = new RoadPanel(gameEngine);
        MAIN_PANEL.add(road, "road");
        MAIN_PANEL.add(new MenuPanel(this.road, gameEngine), "menu");
        MAIN_PANEL.add(new MarketPanel(), "shop");
        MAIN_PANEL.add(new StatisticsPanel(), "stats");
        changePanel("menu");

        setupScene();
    }

    private void setupScene(){
        add(MAIN_PANEL, BorderLayout.CENTER);
        setSize(SCENE_WIDTH, SCENE_HEIGHT);
        setTitle("Late Runner");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        requestFocusInWindow();
        setVisible(true);
    }

    @Override
    public void render() {
        try {
            SwingUtilities.invokeAndWait(this::repaint);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void keyPressed(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.notifyCommand(new MoveRight());
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.notifyCommand(new MoveLeft());
        }
    }

    @Override
    public void keyTyped(final KeyEvent e) { }

    @Override
    public void keyReleased(final KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.notifyCommand(new Stop());
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.notifyCommand(new Stop());
        }
    }

    @Override
    public RoadPanel getRoad() {
        return road;
    }

    /**
     * Switches between panels in order to show the desired one.
     * 
     * @param name
     *          the name of the panel to show
     */
    public static void changePanel(final String name) {
        CARD_LAYOUT.show(MAIN_PANEL, name);
    }
}