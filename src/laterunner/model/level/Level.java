package laterunner.model.level;

import java.util.List;

import laterunner.model.vehicle.Obstacle;

/**
 * The interface in witch is defined the levels.
 *
 */
public interface Level {

    /**
     * Set the list of obstacle of the level.
     * 
     * @param obstacles
     *          the list of obstacle to be set 
     */
    void setObstacles(final List<Obstacle> obstacles);

    /**
     * Get the level's obstacle list. 
     *
     * @return
     *          the level's list
     */
    List<Obstacle> getObstacles();
}
