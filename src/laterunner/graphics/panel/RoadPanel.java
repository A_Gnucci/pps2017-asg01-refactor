package laterunner.graphics.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JLabel;

import laterunner.audio.AudioThemeRunner;
import laterunner.audio.AudioThemeRunnerImpl;
import laterunner.core.GameEngine;
import laterunner.graphics.Icon;
import laterunner.model.user.User;
import laterunner.model.vehicle.*;
import laterunner.model.world.GameState;

/**
 * RoadPanel is the class that contains the real game. It displays all the vehicles and its elements are constantly repainted.
 *
 */
public class RoadPanel extends PanelImpl implements Runnable {

    private static final float FONT_SIZE = 60;
    private static final int CROSS_INITIAL_Y_POSITION = -96;
    private static final int LEFT_CROSS_X_POSITION = 372;
    private static final int RIGHT_CROSS_X_POSITION = 572;
    private static final int CROSS_SPEED = 21;
    private static int crossYPosition = CROSS_INITIAL_Y_POSITION;
    private static final JLabel scoreAndLivesLabel = new JLabel();
    private final AudioThemeRunner audio = new AudioThemeRunnerImpl();
    private final GameEngine gameEngine;
    private GameState gameState;

    /**
     * Common RoadPanel constructor: sets the GameEngine.
     * 
     * @param gameEngine
     *          an instance of GameEngine
     */
    public RoadPanel(final GameEngine gameEngine) {
        this.gameEngine = gameEngine;
        Font font = super.createFont(FONT_SIZE);
        scoreAndLivesLabel.setFont(font);
        scoreAndLivesLabel.setForeground(Color.BLACK);
        this.add(scoreAndLivesLabel, BorderLayout.NORTH);
    }

    @Override
    protected void paintComponent(final Graphics graphics) {
        super.paintComponent(graphics);

        graphics.drawImage(super.getPics().getImage(Icon.ROAD), 0, 0, null);
        this.updateCross();
        graphics.drawImage(super.getPics().getImage(Icon.CROSS), LEFT_CROSS_X_POSITION, crossYPosition, null);
        graphics.drawImage(super.getPics().getImage(Icon.CROSS), RIGHT_CROSS_X_POSITION, crossYPosition, null);

        gameState.getWorld().getSceneEntities().forEach(vehicle -> drawVehicle(vehicle, graphics));
    }

    private void updateCross() {
        crossYPosition += CROSS_SPEED;
        if (crossYPosition >= 0) {
            crossYPosition = CROSS_INITIAL_Y_POSITION;
        }
    }

    private void drawVehicle(Vehicle vehicle, final Graphics graphics){
        String htmlStart = "<html><div style='text-align: center;'>";
        String htmlEnd = "</div></html>";
        if (this.gameEngine.isSurvivalMode()) {
            scoreAndLivesLabel.setText(htmlStart + "Score: " + gameState.getScore() + htmlEnd);
        } else {
            scoreAndLivesLabel.setText(htmlStart + "Lives: " + User.getInstance().getUserLives()  +
                    "&#160; &#160; &#160; &#160; &#160;Score: " + gameState.getScore() + htmlEnd);
        }
        if (vehicle instanceof PlayerCar) {
            PlayerCar car = (PlayerCar) vehicle;
            graphics.drawImage(super.getPics().getImage(Icon.CAR), (int) car.getPosition().getX(),
                    (int) car.getPosition().getY(), null);
        } else if (vehicle instanceof Obstacle) {
            drawObstacle((Obstacle) vehicle, graphics);
        }
    }

    private void drawObstacle(Obstacle obstacle, Graphics graphics){
        if (obstacle.getType() == VehicleType.MOTORBIKE) {
            graphics.drawImage(super.getPics().getImage(Icon.MOTORBIKE), (int) obstacle.getPosition().getX(),
                    (int) obstacle.getPosition().getY(), null);
        } else if (obstacle.getType() == VehicleType.BUS) {
            graphics.drawImage(super.getPics().getImage(Icon.BUS), (int) obstacle.getPosition().getX(),
                    (int) obstacle.getPosition().getY(), null);
        } else if (obstacle.getType() == VehicleType.OBSTACLE_CAR) {
            graphics.drawImage(super.getPics().getImage(Icon.JEEP), (int) obstacle.getPosition().getX(),
                    (int) obstacle.getPosition().getY(), null);
        }
    }

    /**
     * Sets the GameState.
     * @param gmState
     *          an instance of GameState class
     */
    public void setGameState(final GameState gmState) {
        this.gameState = gmState;
    }

    @Override
    public void run() {
        gameEngine.mainLoop();
    }

    /**
     * Returns an instance of AudioThemeRunnerImpl class.
     * @return
     *          an instance of AudioThemeRunnerImpl class
     */
    public AudioThemeRunner getAudio() {
        return this.audio;
    }
}