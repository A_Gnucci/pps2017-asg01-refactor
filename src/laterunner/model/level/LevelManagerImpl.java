package laterunner.model.level;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import laterunner.model.vehicle.VehicleType;
import laterunner.physics.Speed2DImpl;

/**
 * The class that set the levels' features.
 *
 */
public class LevelManagerImpl implements LevelManager {

    private final LevelGenerator levelGenerator = new LevelGeneratorImpl();
    private int distance;
    private Speed2DImpl speed;
    private final List<VehicleType> vehicles = new LinkedList<>();

    /**
     * Based on the level create the level.
     * 
     * @param levelNum
     *          the level to be played
     *
     * @return
     *          the level
     */
    public Level getLevel(final int levelNum) {
        switch(levelNum) {
        case 1:
            this.setLevelFeatures(300, 90, 50, levelNum);
            break;
        case 2:
            this.setLevelFeatures(350, 75, 70, levelNum);
            break;
        case 3:
            this.setLevelFeatures(400, 60, 90, levelNum);
            break;
        case 4:
            this.setLevelFeatures(450, 50, 110, levelNum);
            break;
        case 5:
            this.setLevelFeatures(500, 40, 120, levelNum);
            break;
        case 6:
            this.setLevelFeatures(550, 40, 130, levelNum);
            break;
        case 7:
            this.setLevelFeatures(600, 30, 140, levelNum);
            break;
        case 8:
            this.setLevelFeatures(650, 20, 150, levelNum);
            break;
        case 9:
            this.setLevelFeatures(700, 10, 175, levelNum);
            break;
        case 10:
            this.setLevelFeatures(800, 1, 200, levelNum);
            break;
        default: throw new IllegalStateException();
        }
        return levelGenerator.generateLevel(vehicles, speed, distance);
    }

    private void setLevelFeatures(final int speed, final int distance, final int carsCount, final int level) {
        this.speed = new Speed2DImpl(0, speed);
        this.distance = distance;
        IntStream.range(0, carsCount).forEach(i -> vehicles.add(selectVehicle(level)));
    }

    private VehicleType selectVehicle(final int levelNum) {
        final double TRUCK_CHANCE = 0.03;
        final double CAR_CHANCE = 0.1;
        double random = new Random().nextDouble();

        if (new Random().nextDouble() <= TRUCK_CHANCE * levelNum) {
            return VehicleType.BUS;
        }
        if (random > TRUCK_CHANCE * levelNum && random <= CAR_CHANCE * levelNum) {
            return VehicleType.OBSTACLE_CAR;
        }
        return VehicleType.MOTORBIKE;
    }
}