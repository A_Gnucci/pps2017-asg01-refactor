package laterunner.input;

import laterunner.model.world.GameState;
import laterunner.physics.Speed2DImpl;

/**
 * Stop command class.
 */
public class Stop implements Command {

    /**
     * Stops car movement.
     * @param gameState
     *          game coordinator
     */
    public void execute(final GameState gameState) {
        gameState.getWorld().getPlayerCar().setSpeed(new Speed2DImpl(0, 0));
    }
}
