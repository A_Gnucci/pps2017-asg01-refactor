package laterunner.core;

import laterunner.graphics.Icon;
import laterunner.graphics.PictureManagerImpl;
import laterunner.model.vehicle.VehicleType;

/**
 * Dimensions class.
 */
public final class Dimensions {

    private static final int WIDTH_JEEP = 21;
    private static final int WIDTH_BUS = 20;
    private static final int WIDTH_MOTORBIKE = 15;
    private static final int WIDTH_CAR = 12;
    private static final int HEIGHT_JEEP = 21;
    private static final int HEIGHT_BUS = 26;
    private static final int HEIGHT_MOTORBIKE = 47;
    
    private static final PictureManagerImpl icons = new PictureManagerImpl();
    private static final Dimensions instance = new Dimensions();

    private Dimensions() { }

    /**
     * Instantiates dimensions with a lazy initialization.
     * @return
     *          The only one instance
     */
    public static Dimensions getInstance() {
        return instance;
    }

    /**
     * Calculate the right vehicle's width.
     * 
     * @param vehicle
     *          vehicle used to select the right one obstacle
     * @return 
     *          width
     */

    public double getVehicleWidth(final VehicleType vehicle) {
        switch (vehicle) {
            case OBSTACLE_CAR : return icons.getIcon(Icon.JEEP).getIconWidth() - WIDTH_JEEP;
            case BUS : return  icons.getIcon(Icon.BUS).getIconWidth() - WIDTH_BUS;
            case MOTORBIKE : return icons.getIcon(Icon.MOTORBIKE).getIconWidth() - WIDTH_MOTORBIKE;
            case USER_CAR : return icons.getIcon(Icon.CAR).getIconWidth() - WIDTH_CAR;
            default : throw new IllegalStateException();
        }
}

    /**
     * Calculate the right vehicle's height.
     * 
     * @param vehicle
     *          vehicle used to select the right one obstacle
     * @return 
     *          height
     */
    public double getVehicleHeight(final VehicleType vehicle) {
        switch (vehicle) {
            case OBSTACLE_CAR : return icons.getIcon(Icon.JEEP).getIconHeight() - HEIGHT_JEEP;
            case BUS : return  icons.getIcon(Icon.BUS).getIconHeight() - HEIGHT_BUS;
            case MOTORBIKE : return icons.getIcon(Icon.MOTORBIKE).getIconHeight() - HEIGHT_MOTORBIKE;
            case USER_CAR : return icons.getIcon(Icon.CAR).getIconHeight();
            default : throw new IllegalStateException();
        }
    }
}