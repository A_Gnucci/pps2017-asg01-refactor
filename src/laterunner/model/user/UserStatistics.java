package laterunner.model.user;

public interface UserStatistics {

    /**
     * Returns motorbike hits' number.
     *
     * @return
     *          motorbike hits' number
     */
    long getMotorbikeHits();

    /**
     * Sets new motorbike hits' number.
     *
     * @param bikeHits
     *          new motorbike hits' number
     */
    void setMotorbikeHits(final long bikeHits);

    /**
     * Returns obstacle car hits' number.
     *
     * @return
     *          obstacle car hits' number
     */
    long getCarHits();

    /**
     * Sets new obstacle car hits' number.
     *
     * @param carHits
     *          new obstacle car hits' number
     */
    void setCarHits(final long carHits);

    /**
     * Returns truck hits' number.
     *
     * @return
     *          truck hits' number
     */
    long getTruckHits();

    /**
     * Sets new truck hits' number.
     *
     * @param truckHits
     *          new truck hits' number
     */
    void setTruckHits(final long truckHits);

    /**
     * Returns games played number.
     *
     * @return
     *          games played number
     */
    long getGamesPlayed();

    /**
     * Sets new games played number.
     *
     * @param gamesPlayed
     *          new games played number
     */
    void setGamesPlayed(final long gamesPlayed);

    /**
     * Returns lost lives' number.
     *
     * @return
     *          lost lives' number
     */
    long getLostLives();

    /**
     * Sets lost lives' number.
     *
     * @param lstLives
     *          new lost lives' number
     */
    void setLostLives(final long lstLives);

    /**
     * Returns survival score.
     *
     * @return
     *          survival score number
     */
    long getSurvivalScore();

    /**
     * Sets survival score number.
     *
     * @param score
     *          new survival score number
     */
    void setSurvivalScore(final long score);
}
