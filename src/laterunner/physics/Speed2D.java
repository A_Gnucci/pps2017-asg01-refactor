package laterunner.physics;

public interface Speed2D extends java.io.Serializable {

    /**
     * Multiply the speed by factor.
     *
     * @param factor
     *          parameter to multiply speed to
     * @return
     *          speed multiply factor
     */
    Speed2D multiply(final double factor);

    /**
     * Get the X component.
     *
     * @return
     *          the X component
     */
    double getXSpeed();

    /**
     * Get the Y component.
     *
     * @return
     *          the Y component
     */
    double getYSpeed();
}
