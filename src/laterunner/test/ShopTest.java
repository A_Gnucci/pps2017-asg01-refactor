package laterunner.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import laterunner.model.shop.Shop;
import laterunner.model.user.User;

/**
 * Shop tester.
 *
 */
public class ShopTest {

    private static final Shop shop = Shop.getInstance();
    private static final User user = User.getInstance();

    @Before
    public void beforeEach(){
        user.reset();
        shop.reset();
    }

    @Test
    public void testLifeCost(){
        shop.buyLife();
        assertEquals(1500, shop.getLifeCost());
        user.setMoney(user.getMoney() + 5000);
        shop.buyLife();
        assertEquals(2250, shop.getLifeCost());

        shop.reset();
        assertEquals(1000, shop.getLifeCost());
    }

    @Test
    public void testSpeedCost(){
        user.setMoney(user.getMoney() + 5000);
        shop.buySpeed();
        assertEquals(6000, shop.getSpeedCost());

        user.reset();
        shop.reset();
        assertEquals(3000, shop.getSpeedCost());

        user.setMoney(user.getMoney() + 21000);
        shop.buySpeed();
        shop.buySpeed();
        assertEquals(12000, shop.getSpeedCost());
    }

    @Test
    public void testUserLives(){
        shop.buyLife();
        shop.buyLife();
        assertEquals(4, User.getInstance().getUserLives());
    }

    @Test
    public void testSpeedMultiplier(){
        shop.buySpeed();
        assertEquals(1.0, User.getInstance().getSpeedMultiplier(), 0.0);
        user.reset();
        user.setMoney(user.getMoney() + 21000);
        shop.reset();
        shop.buySpeed();
        shop.buySpeed();
        shop.buySpeed();
        assertEquals(2.0, User.getInstance().getSpeedMultiplier(), 0.0);
    }
 }
