package laterunner.model.world;

import laterunner.model.collisions.WorldEventListener;

public class GameStateImplBuilder {
    private WorldEventListener worldEventListener;
    private int level;
    private int score;

    public GameStateImplBuilder setWorldEventListener(WorldEventListener worldEventListener) {
        this.worldEventListener = worldEventListener;
        return this;
    }

    public GameStateImplBuilder setLevel(int level) {
        this.level = level;
        return this;
    }

    public GameStateImplBuilder setScore(int score) {
        this.score = score;
        return this;
    }

    public GameStateImpl build() {
        return new GameStateImpl(worldEventListener, level, score);
    }
}