package laterunner.model.vehicle;

import laterunner.physics.Position2DImpl;
import laterunner.physics.Speed2D;
import laterunner.physics.Speed2DImpl;

/**
 * User's PlayerCar.
 */
public class PlayerCar extends VehicleImpl {

    private static final int X_POS = 435;
    private static final int Y_POS = 510;

    /**
     * Instantiates a new PlayerCar.
     */
    public PlayerCar() {
        super(new Position2DImpl(X_POS, Y_POS), new Speed2DImpl(0, 0), VehicleType.USER_CAR);
    }

    @Override
    public void setSpeed(final Speed2D speed) {
        super.setCheckedSpeed(new Speed2DImpl(speed.getXSpeed(), 0));
    }
}
