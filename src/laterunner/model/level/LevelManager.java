package laterunner.model.level;

public interface LevelManager {

    Level getLevel(final int levelNum);
}
