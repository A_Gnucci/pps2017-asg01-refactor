package laterunner.graphics;

import laterunner.graphics.panel.RoadPanel;

/**
 * Scene is the main JFrame that contains every panel.
 *
 */
public interface Scene {

    /**
     * Invokes paintComponent.
     */
    void render();

    /**
     * Gets an instance of RoadPanel.
     * 
     * @return
     *          an instance of RoadPanel class
     */
    RoadPanel getRoad();

}
