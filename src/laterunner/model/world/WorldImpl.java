package laterunner.model.world;

import java.util.*;

import laterunner.core.Dimensions;
import laterunner.model.collisions.BorderBoundingBox;
import laterunner.model.collisions.BorderHitEvent;
import laterunner.model.collisions.ObstacleHitEvent;
import laterunner.model.collisions.WorldEventListener;
import laterunner.model.level.LevelManager;
import laterunner.model.level.LevelManagerImpl;
import laterunner.model.vehicle.*;

/**
 * The class in which the world features are implemented
 *
 */
public class WorldImpl implements World {

    private static final int LOWER_BORDER_Y = 720;
    private static final int BORDER_DAMAGE = 500;

    private final List<Obstacle> obstacles;
    private final LevelManager levelManager;
    private Vehicle playerCar;
    private final BorderBoundingBox mainBoundingBox;
    private WorldEventListener eventListener;

    /**
     * 
     * @param boundingBox
     *          the bounding box to be set
     */
    public WorldImpl(final BorderBoundingBox boundingBox) {
        obstacles = new LinkedList<>();
        levelManager = new LevelManagerImpl();
        mainBoundingBox = boundingBox;
    }

    @Override
    public void setEventListener(final WorldEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public void removeObstacle(final Obstacle obstacle) {
        this.obstacles.remove(obstacle);
    }

    @Override
    public List<Vehicle> getSceneEntities() {
        List<Vehicle> entities = new ArrayList<>(obstacles);
        entities.add(playerCar);
        return entities;
    }

    @Override
    public void updateState(final int elapsed) {
        Iterator<Obstacle> iterator = obstacles.iterator();
        while (iterator.hasNext()){
            Obstacle obstacle = iterator.next();
            obstacle.updateState(elapsed);
            if (obstacle.getPosition().getY() > LOWER_BORDER_Y) {
                iterator.remove();
            }
        }
        playerCar.updateState(elapsed);
        checkBoundaries();
        checkCollisions();
    }

    private void checkBoundaries() {
        if (this.mainBoundingBox.isCollidingWith(getPlayerCar())) {
            eventListener.notifyEvent(new BorderHitEvent());
        }
    }

    private void checkCollisions() {
        obstacles.stream()
                .filter(this::isObstacleColliding)
                .findAny()
                .ifPresent(obstacle1 -> eventListener.notifyEvent(new ObstacleHitEvent(obstacle1)));
    }

    private boolean isObstacleColliding(Obstacle obstacle){
        return obstacle.getPosition().getY() + Dimensions.getInstance().getVehicleHeight(obstacle.getType()) >=
                (playerCar.getPosition().getY() + 2) && obstacle.getPosition().getY() <= playerCar.getPosition().getY()
                + Dimensions.getInstance().getVehicleHeight(VehicleType.USER_CAR) &&
                obstacle.getBoundingBox().isCollidingWith(getPlayerCar());
    }

    @Override
    public void generateLevel(final int i) {
       this.playerCar =  new VehicleFactoryImpl().createPlayerCar();
       this.obstacles.addAll((this.levelManager.getLevel(i)).getObstacles());
    }

    @Override
    public Vehicle getPlayerCar() {
        return playerCar;
    }

    @Override
    public int getBorderDamage() {
        return BORDER_DAMAGE;
    }
}