package laterunner.model.vehicle;

import laterunner.physics.Position2D;
import laterunner.physics.Speed2D;

public interface VehicleFactory {

    Vehicle createPlayerCar();
    Obstacle createObstacleCar(Position2D position, Speed2D speed);
    Obstacle createObstacleBike(Position2D position, Speed2D speed);
    Obstacle createObstacleBus(Position2D position, Speed2D speed);
    Obstacle createObstacleByType(VehicleType type, Position2D position, Speed2D speed);

}
