package laterunner.physics;

/**
 * Position on screen.
 */
public class Position2DImpl implements Position2D {

    private static final long serialVersionUID = 1L;

    private double x;
    private double y;

    /**
     * 
     * @param x
     *          param to set x
     * @param y
     *          param to set y
     */
    public Position2DImpl(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Translate the point.
     * 
     * @param vector
     *          the vector to translate the point
     * @return
     *          the point after the translation
     */
    public Position2D sum(final Speed2D vector) {
        return new Position2DImpl(x + vector.getXSpeed(), y + vector.getYSpeed());
    }

    @Override
    public String toString() {
        return "Position2DImpl(" + x + "," + y + ")";
    }

    /**
     * Get the x component.
     * 
     * @return
     *          the x component
     */
    public double getX() {
        return x;
    }

    /**
     * Set the x component.
     * 
     * @param x
     *          the new x component
     */
    public void setX(final double x) {
        this.x = x;
    }

    /**
     * Get the y component.
     * 
     * @return
     *          the y component
     */
    public double getY() {
        return y;
    }

    /**
     * Set the y component.
     * 
     * @param y
     *          the new y component
     */
    public void setY(final double y) {
        this.y = y;
    }
}

