package laterunner.model.shop;

import laterunner.model.saving.FileManager;
import laterunner.model.user.User;

/**
 * The class in witch is implemented the Shop.
 *
 */
public final class Shop {

    private static final int INITIAL_LIFE_COST = 1000;
    private static final int INITIAL_SPEED_COST = 3000;
    private static final double LIFE__COST_MULTIPLIER = 1.5;
    private static final Shop instance = new Shop();
    private int lifeCost = INITIAL_LIFE_COST;
    private int speedCost = INITIAL_SPEED_COST;


    private Shop() {
        reset();
    }

    /**
     * Get the only instance of Shop. 
     * 
     * @return
     *          the only instance of Shop
     */
    public static Shop getInstance() {
            return instance;
    }

    /**
     * The method to reset the Shop features.
     * 
     */
    public void reset() {
        this.lifeCost = INITIAL_LIFE_COST;
        this.speedCost = INITIAL_SPEED_COST;
    }

    /**
     * The method in which you can buy a life.
     * 
     */
    public void buyLife() {
        if (!this.isEnableLife()) {
            return;
        }
        User.getInstance().setMoney(User.getInstance().getMoney() - lifeCost);
        User.getInstance().setUserLives(User.getInstance().getUserLives() + 1);
        this.lifeCost *= LIFE__COST_MULTIPLIER;
        FileManager.saveToFile();
    }

    /**
     * The method in witch you can increase your speed.
     * 
     */
    public void buySpeed() {
        if (!this.isEnableSpeed()) {
            return;
        }
        User.getInstance().setMoney(User.getInstance().getMoney() - speedCost);
        User.getInstance().setSpeedMultiplier(User.getInstance().getSpeedMultiplier() + 0.5);
        this.speedCost *= 2;
        FileManager.saveToFile();
    }

    /**
     * Return the actual life cost.
     * 
     * @return
     *          the life cost
     */
    public int getLifeCost() {
            return lifeCost;
    }

    /**
     * Get the actual speed cost.
     * 
     * @return
     *          the speed cost
     */
    public int getSpeedCost() {
            return speedCost;
    }

    /**
     * Set the life cost to lifeCost.
     * 
     * @param lifeCost
     *          the new life cost
     */
    public void setLifeCost(final int lifeCost) {
            this.lifeCost = lifeCost;
    }

    /**
     * Set the speed cost to speedCost.
     * 
     * @param speedCost
     *          the new speed cost
     */
    public void setSpeedCost(final int speedCost) {
            this.speedCost = speedCost;
    }

    /**
     * Check if is possible buy a life.
     * 
     * @return
     *              true if is possible buy a life
     */
    private boolean isEnableLife() {
            return User.getInstance().getMoney() - lifeCost >= 0;
    }

    /**
     * Check if is possible buy speed.
     * 
     * @return
     *          true if is possible buy speed
     */
    private boolean isEnableSpeed() {
            return User.getInstance().getSpeedMultiplier() < 2 && User.getInstance().getMoney() - speedCost >= 0;
    }
}