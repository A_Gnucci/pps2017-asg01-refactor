package laterunner.model.level;

import laterunner.model.vehicle.Obstacle;

import java.util.LinkedList;
import java.util.List;

/**
 * The class in witch is implemented the leves.
 *
 */
public class LevelImpl implements Level {

    private final List<Obstacle> level = new LinkedList<>();

    @Override
    public void setObstacles(final List<Obstacle> list) {
        this.level.addAll(list);
    }

    @Override
    public List<Obstacle> getObstacles() {
        return level;
    }
}
